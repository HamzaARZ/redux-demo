import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
import { createStore } from 'redux';

import reducers from './reducers';



let store = createStore(reducers);

/*
import {createStore} from 'redux';

// Actions
const increment = () => {
  return {
    type: 'INCREMENT'
  }
};
const decrement = () => {
  return {
    type: 'DECREMENT'
  }
};
const incrementBy = (value) => {
  return {
    type: 'INCREMENT_BY',
    value: value
  }
}
const decrementBy = (value) => {
  return {
    type: 'DECREMENT_BY',
    value: value
  }
}


// Reducer
const initState = 0;

const counter = (state=initState, action) => {
  switch(action.type) {
    case 'INCREMENT':
      return state + 1;
    case 'DECREMENT':
      return state - 1;
    case 'INCREMENT_BY':
      return state + action.value;
    case 'DECREMENT_BY':
      return state - action.value;

  }
}

// Create store
let store = createStore(counter);


// Subscribe to the store
store.subscribe(() => console.log(store.getState()));


// Dsipatch actions
store.dispatch(increment());
store.dispatch(incrementBy(5));
store.dispatch(decrement());
store.dispatch(decrementBy(2));

*/

ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
